import numpy as np
import pandas as pd

def linReg(src, period=14):
    
  x = pd.Series(np.arange(len(src)))
  y = src
  xMean = x.rolling(window=period).mean()
  yMean = y.rolling(window=period).mean()
  xStd = x.rolling(window=period).std()
  yStd = y.rolling(window=period).std()
  corr = x.rolling(window=period).corr(y)

  slope = corr * (yStd/xStd)
  intercept = yMean - slope * xMean
  curve = x * slope + intercept

  return [slope, intercept, curve]


def linRegSlope(src, period):
  return linReg(src, period)[0]

def linRegIntercept(src, period):
  return linReg(src, period)[1]

def linRegCurve(src, period):
  return linReg(src, period)[2]

def lrc(src, period):
    return pd.Series(linRegCurve(src, period)) 

def lrs(src, period):
    curve = linRegCurve(src, period)
    return pd.Series((curve-curve.shift(1))/1)

def slrs(src, period, adjust = False, ignore_na = False):
    # uacce = lrs > alrs and lrs > 0 
    # dacce = lrs < alrs and lrs < 0 
    curve = linRegCurve(src, period)
    lrs = (curve-curve.shift(1)) / 1
    _slrs = lrs.ewm(span=period, adjust=adjust, ignore_na=ignore_na).mean()
    return pd.Series(_slrs) 

def slrsNorm(src, period, adjust=False, ignore_na=False):
    curve = linRegCurve(src, period)
    lrs = (curve-curve.shift(1)) / 1
    slrs_ = lrs.ewm(span=period, adjust=adjust, ignore_na=ignore_na).mean()
    norm = normalize(slrs_, period)
    return pd.Series(norm)

def alrs(src, period1, period2, adjust=False, ignore_na=False):
    curve = linRegCurve(src, period1)
    lrs = (curve-curve.shift(1)) / 1
    slrs = lrs.ewm(span=period1, adjust=adjust, ignore_na=ignore_na).mean()
    alrs = slrs.rolling(window=period2).mean()
    return pd.Series(alrs)

def lrcSlope(src, period):
    return pd.Series(linregSlope(src, period)) 

def lrcIntercept(src, period):
    curve = linRegCurve(src, period)
    slope = linregSlope(src, period)
    return pd.Series(curve - period * slope)

def lrcInterceptNorm(lrcIntercept, period):
     norm = normalize(lrcIntercept, period)
     return pd.Series(norm)

def lrc1(src, period, offset):
    curve = linRegCurve(src, period)
    slope = linregSlope(src, period)
    intercept = curve - period * slope
    return pd.Series(intercept + slope * (period  - offset))

def lrc1Norm(lrc1, period):
    norm = normalize(lrc1, period)
    return pd.Series(norm)

def lrcChannel(src, period):
    lrc = linRegCurve(src, period)
    u2 = lrc + 2 * src.rolling(window=period).std(ddof=0)
    u1 = lrc + 1 * src.rolling(window=period).std(ddof=0)
    l2 = lrc - 2 * src.rolling(window=period).std(ddof=0)
    l1 = lrc - 1 * src.rolling(window=period).std(ddof=0)
    return [lrc,u2,u1,l1,21]

# Daily - LRS-5 is greater than ALRS-5
# Daily - LRS-5 is less than 0
# Daily - SLRS-5 is less than 0
# 1H - SLRS-5 greater than previous SLRS-5

df = pdr.DataReader('AAPL', 'yahoo', '2020-01-01', '2021-12-31')
df.reset_index(inplace=True)
df['lrs5'] = lrs(df.Close, 5)
df['alrs5'] = alrs(df.Close, 5, 13)
df['slrs5'] = slrs(df.Close, 5)


(df.lrs5 > df.alrs5) &
(df.lrs5 < 0) &  
(df.slrs5 < 0) &  
