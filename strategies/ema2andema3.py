import numpy as np
import pandas as pd

def linReg(src, period=14):
    
  x = pd.Series(np.arange(len(src)))
  y = src
  xMean = x.rolling(window=period).mean()
  yMean = y.rolling(window=period).mean()
  xStd = x.rolling(window=period).std()
  yStd = y.rolling(window=period).std()
  corr = x.rolling(window=period).corr(y)

  slope = corr * (yStd/xStd)
  intercept = yMean - slope * xMean
  curve = x * slope + intercept

  return [slope, intercept, curve]


def linRegSlope(src, period):
  return linReg(src, period)[0]

def linRegIntercept(src, period):
  return linReg(src, period)[1]

def linRegCurve(src, period):
  return linReg(src, period)[2]

def slrs(src, period, adjust = False, ignore_na = False):
    # uacce = lrs > alrs and lrs > 0 
    # dacce = lrs < alrs and lrs < 0 
    curve = linRegCurve(src, period)
    lrs = (curve-curve.shift(1)) / 1
    _slrs = lrs.ewm(span=period, adjust=adjust, ignore_na=ignore_na).mean()
    return pd.Series(_slrs) 

def ema(src, period, adjust=False, ignore_na=False):
    
    ema = pd.Series(src.ewm(span=period, adjust=adjust, ignore_na=ignore_na).mean())

    return ema

df = pdr.DataReader('AAPL', 'yahoo', '2020-01-01', '2021-12-31')
df.reset_index(inplace=True)

ema2c = ema(df.Close, 2)
ema3c = ema(df.Close, 3)
slrs5c = slrs(df.Close, 5)

# slrs5 is greater than previous slrs5
# ema2 is greater than previous ema2
# previous ema2 is less than ema2 from day 2
# slrs5 is less than 0

strategy = df[
 (slrs5c > slrs5c.shift(1)) & 
 (ema2c > ema2c.shift(1)) &
 (ema2c.shift(1) < ema2c.shift(2)) &
 (slrs5c < 0)
 ]
