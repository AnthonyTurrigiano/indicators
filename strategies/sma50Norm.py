import pandas as pd

def sma(src, period):
    
    sma = pd.Series(src.rolling(window=period).mean())
    return sma

def normalize(src, period):
  min = src.rolling(window=period).min()
  max = src.rolling(window=period).max()

  return ((src - min) / (max-min)* 100)

df = pdr.DataReader('AAPL', 'yahoo', '2020-01-01', '2021-12-31')
df.reset_index(inplace=True)

# Previous bar is 0 
# Current bar greater than 5
# Sell when bar equals 100

df['sma20'] = sma(df.Close, 50)
df['sma20Norm'] = normalize(df.sma50, 5)


