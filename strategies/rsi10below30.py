import pandas as pd
import pandas_datareader as pdr

# Description: Relative Strength Indicator
def rsi(src, period=14):

  # Calculate the difference in price from previous bar
  delta = src.diff()

  # Copy delta
  up, down = delta.copy(), delta.copy()

  # If up is less than 0 up equals 0
  up[up <  0] = 0

  # If down is less than 0 down equals 0
  down[down > 0] = 0

  # up and down moving averages
  gain = up.ewm(alpha=1.0/period, adjust=True).mean()
  loss = down.abs().ewm(alpha=1.0/period, adjust=True).mean()

  # Relative Strength
  rs = gain / loss

  # Relative Strength Index
  rsi = pd.Series(100-(100/(1+rs))) 
  
  return rsi

def sma(src, period):
    
    sma = pd.Series(src.rolling(window=period).mean())
    return sma

df = pdr.DataReader('AAPL', 'yahoo', '2020-01-01', '2021-12-31')
df.reset_index(inplace=True)

close = df.Close
sma200c =sma(df.Close, 200)
rsi10c = rsi(df.Close, 10)

# Buy next day open when close is greater than 200 ma and rsi10 is below 30 
# Sell when rsi10 hits 40 

strategy = df
[
  (close > sma200c) & 
  (rsi10c.shift(1) < 30) & 
  (rsi10c.shift(2) > 30)
  ]

