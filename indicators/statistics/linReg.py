import numpy as np
import pandas as pd

def linReg(src, period=14):
    
  x = pd.Series(np.arange(len(src)))
  y = src
  xMean = x.rolling(window=period).mean()
  yMean = y.rolling(window=period).mean()
  xStd = x.rolling(window=period).std()
  yStd = y.rolling(window=period).std()
  corr = x.rolling(window=period).corr(y)

  slope = corr * (yStd/xStd)
  intercept = yMean - slope * xMean
  curve = x * slope + intercept

  return [slope, intercept, curve]


def linRegSlope(src, period):
  return linReg(src, period)[0]

def linRegIntercept(src, period):
  return linReg(src, period)[1]

def linRegCurve(src, period):
  return linReg(src, period)[2]
