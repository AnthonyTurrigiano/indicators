def lrc(src, period):
    return pd.Series(linRegCurve(src, period)) 

def lrs(src, period):
    curve = linRegCurve(src, period)
    return pd.Series((curve-curve.shift(1))/1)

def slrs(src, period, adjust = False, ignore_na = False):
    # uacce = lrs > alrs and lrs > 0 
    # dacce = lrs < alrs and lrs < 0 
    curve = linRegCurve(src, period)
    lrs = (curve-curve.shift(1)) / 1
    _slrs = lrs.ewm(span=period, adjust=adjust, ignore_na=ignore_na).mean()
    return pd.Series(_slrs) 

def slrsNorm(src, period, adjust=False, ignore_na=False):
    curve = linRegCurve(src, period)
    lrs = (curve-curve.shift(1)) / 1
    slrs_ = lrs.ewm(span=period, adjust=adjust, ignore_na=ignore_na).mean()
    norm = normalize(slrs_, period)
    return pd.Series(norm)

def alrs(src, period1, period2, adjust=False, ignore_na=False):
    curve = linRegCurve(src, period1)
    lrs = (curve-curve.shift(1)) / 1
    slrs = lrs.ewm(span=period1, adjust=adjust, ignore_na=ignore_na).mean()
    alrs = slrs.rolling(window=period2).mean()
    return pd.Series(alrs)

def lrcSlope(src, period):
    return pd.Series(linregSlope(src, period)) 

def lrcIntercept(src, period):
    curve = linRegCurve(src, period)
    slope = linregSlope(src, period)
    return pd.Series(curve - period * slope)

def lrcInterceptNorm(lrcIntercept, period):
     norm = normalize(lrcIntercept, period)
     return pd.Series(norm)

def lrc1(src, period, offset):
    curve = linRegCurve(src, period)
    slope = linregSlope(src, period)
    intercept = curve - period * slope
    return pd.Series(intercept + slope * (period  - offset))

def lrc1Norm(lrc1, period):
    norm = normalize(lrc1, period)
    return pd.Series(norm)

def lrcChannel(src, period):
    lrc = linRegCurve(src, period)
    u2 = lrc + 2 * src.rolling(window=period).std(ddof=0)
    u1 = lrc + 1 * src.rolling(window=period).std(ddof=0)
    l2 = lrc - 2 * src.rolling(window=period).std(ddof=0)
    l1 = lrc - 1 * src.rolling(window=period).std(ddof=0)
    return [lrc,u2,u1,l1,21]
