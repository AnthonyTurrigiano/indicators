def highest(src, period=9):
  return src.rolling(window=period).max()
