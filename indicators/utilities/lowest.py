def lowest(src, period=9):
  return src.rolling(window=period).min()
