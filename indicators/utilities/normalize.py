def normalize(src, period):
  min = src.rolling(window=period).min()
  max = src.rolling(window=period).max()

  return ((src - min) / (max-min)* 100)
