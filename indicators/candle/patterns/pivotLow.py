import numpy as np
import pandas as pd

def pivotLow(src):
    return pd.Series(np.where( (src < src.shift(1)) & (src < src.shift(-1)), src, 0))
