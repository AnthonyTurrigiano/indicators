import numpy as np
import pandas as pd

def candleIsBullishHammer(open, high, low, close, period=50, candleShadowPct=5.0):
  candleBodyHigh = pd.Series(np.maximum(close, open))
  candleBodyLow =  pd.Series(np.minimum(close, open))
  candleBody = candleBodyHigh-candleBodyLow
  candleBodyMA = candleBody.ewm(span=period, adjust=adjust, ignore_na=ignore_na).mean()
  candleIsSmallBody = candleBody < candleBodyMA
  candleDownShadow = low - candleBodyLow
  candleUpShadow = high - candleBodyHigh
  candleHasUpShadow = candleUpShadow > candleShadowPct / 100 * candleBody  
  ma = close.rolling(window=period).mean()
  candleIsDownTrend = close < ma
  candleIsBullishHammer = pd.Series(np.where(
        (candleIsSmallBody > 0) &
        (candleBody > 0) &
        (candleBodyLow > ((high+low) / 2)) &
        (candleDnShadow >= ((2 * candleBody))) &         
        (candleHasUpShadow == False) &
        (candleIsDnTrend == True)        
        , True, False))

  return candleIsBullishHammer
