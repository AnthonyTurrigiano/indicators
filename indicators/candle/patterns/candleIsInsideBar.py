import numpy as np
import pandas as pd

def candleIsInsideBar(open, close):
    
    candleBodyHigh = pd.Series(np.maximum(close, open))
    candleBodyLow =  pd.Series(np.minimum(close, open))
    candleIsInsideBar = (candleBodyHigh.shift(1) > candleBodyHigh) & (candleBodyLow.shift(1) < candleBodyLow)

    return pd.Series(candleIsInsideBar)
