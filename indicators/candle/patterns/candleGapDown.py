import pandas as pd

def candleIsGapDow(low, high):
  return pd.Series(low.shift(1) > high)
