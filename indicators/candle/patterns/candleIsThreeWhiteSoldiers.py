import numpy as np
import pandas as pd

def candleIsThreeWhiteSholders(open, high, low, close, period = 14, candleShadowPct = 5):
  candleBodyHigh = pd.Series(np.maximum(close, open))
  candleBodyLow =  pd.Series(np.minimum(close, open))
  candleBody = candleBodyHigh-candleBodyLow
  candleBodyMA = candleBody.ewm(span=period, adjust=adjust, ignore_na=ignore_na).mean()
  candleIsLongBody = candleBody > candleBodyMA
  candleIsWhiteBody = open < close
  candleRange = high-low
  candleUpShadow = pd.Series(high - candleBodyHigh)
  candleDoesNotHaveShadow = candleRange * candleShadowPct / 100 > candleUpShadow
  candleIsThreeWhiteSholders = pd.Series(np.where(
      (candleBody > 0) &
      (candleIsLongBody > 0) & 
      (candleIsLongBody.shift(1) > 0) & 
      (candleIsLongBody.shift(2) > 0) & 
      (candleIsWhiteBody > 0) & 
       (candleIsWhiteBody.shift(1) > 0) & 
       (candleIsWhiteBody.shift(2) > 0) &
       (close > close.shift(1)) & 
       (close.shift(1) > close.shift(2)) & 
       (open < close.shift(1) ) & 
       (open > open.shift(1)) & 
       (open.shift(1) < close.shift(2)) & 
       (open.shift(1) > open.shift(1))
  ,True, False))

  return candleIsThreeWhiteSholders
