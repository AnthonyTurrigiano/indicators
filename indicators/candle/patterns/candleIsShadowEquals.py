import pandas as pd

def candleIsShadowEquals(open, close, candleShadowEqualsPct=100)
  candleBodyHigh = np.maximum(close, open)
  candleBodyLow = np.minimum(close, open)
  candleUpShadow = pd.Series(high - candleBodyHigh)
  candleDownShadow = pd.Series(low - candleBodyLow)
  candleIsShadowEquals = (candleUpShadow == candleDownShadow) | (((abs(candleUpShadow - candleDownShadow) / candleDownShadow * 100) < candleShadowEqualsPct) & (abs(candleDownShadow-candleUpShadow) / candleUpShadow * 100) < candleShadowEqualsPct)

  return pd.Series(candleIsShadowEquals)
