import pandas as pd

def candleIsGapUp(low, high):
  return pd.Series(low > high.shift(1))
