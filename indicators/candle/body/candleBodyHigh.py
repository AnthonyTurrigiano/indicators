import pandas as pd
import numpy as np

def candleBodyHigh(open, close):
    candleBodyHigh = np.maximum(close, open)
    
    return pd.Series(candleBodyHigh)
