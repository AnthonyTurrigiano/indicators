import pandas as pd
import numpy as np

def candleBodyLow(open, close):
    return pd.Series(np.minimum(close, open))
