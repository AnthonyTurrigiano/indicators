import numpy as np
import pandas as pd

def candleBody(open, close):
    
    candleBodyHigh = pd.Series(np.maximum(close, open))
    candleBodyLow =  pd.Series(np.minimum(close, open))
    candleBody = candleBodyHigh-candleBodyLow

    return pd.Series(candleBody)
