import pandas as pd

def candleBodyMA(open, close, period=14, adjust=False, ignore_na=False):
  
  candleBodyHigh = pd.Series(np.maximum(close, open))
  candleBodyLow =  pd.Series(np.minimum(close, open))
  candleBody = candleBodyHigh-candleBodyLow
  candleBodyMA = candleBody.ewm(span=period, adjust=adjust, ignore_na=ignore_na).mean()

  return pd.Series(candleBodyMA)
