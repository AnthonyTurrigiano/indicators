import pandas as pd

def candleIsLongBody(open, close):
    
    candleBodyHigh = pd.Series(np.maximum(close, open))
    candleBodyLow =  pd.Series(np.minimum(close, open))
    candleBody = candleBodyHigh - candleBodyLow
    candleBodyMA = candleBody.ewm(span=period, adjust=False, ignore_na=False).mean()
    candleIsLongBody = candleBody > candleBodyMA

    return pd.Series(candleIsLongBody)
