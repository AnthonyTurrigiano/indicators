import pandas as pd

def candleMidPoint(high, low):

    candleMidPoint = (high + low) / 2

    return candleMidPoint
