import pandas as pd

def candleBodyMidPoint(open, close):

    candleBodyHigh = pd.Series(np.maximum(close, open))
    candleBodyLow =  pd.Series(np.minimum(close, open))
    candleBodyMidPoint = (candleBodyHigh + candleBodyLow) / 2
    
    return pd.Series(candleBodyMidPoint)
    
