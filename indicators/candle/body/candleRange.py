import pandas as pd

def candleRange(high, low):

    candleRange = high-low
    
    return pd.Series(candleRange)
