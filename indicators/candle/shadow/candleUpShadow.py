import pandas as pd
import numpy as np

def candleUpShadow(open, high, close)
    candleBodyHigh = np.maximum(close, open)
    candleUpShadow = pd.Series(high - candleBodyHigh)

    return candleUpShadow
