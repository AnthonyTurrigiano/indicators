def candleDoesNotHaveShadow(open, high, low, high, candleShadowPct = 5):
    candleRange = high - low
    candleBodyHigh = np.maximum(close, open)
    candleUpShadow = pd.Series(high - candleBodyHigh)
    candleDoesNotHaveShadow = candleRange * candleShadowPct / 100 > candleUpShadow

    return candleDoesNotHaveShadow
