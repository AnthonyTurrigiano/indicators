import pandas as pd
import numpy as np

def candleDownShadow(open, low, close)
    candleBodyLow = np.minimum(close, open)
    candleDownShadow = pd.Series(low - candleBodyLow)

    return candleDownShadow

