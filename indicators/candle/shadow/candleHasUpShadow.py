import numpy as np
import pandas as pd

def candleHasUpShadow(open, close, high, shadowPct=5.0):
    candleBodyHigh = pd.Series(np.maximum(close, open))
    candleBodyLow =  pd.Series(np.minimum(close, open))
    candleBody = candleBodyHigh - candleBodyLow
    candleUpShadow = pd.Series(high - candleBodyHigh)
    candleHasUpShadow = candleUpShadow > shadowPct / 100 * candleBody
    return pd.Series(candleHasUpShadow)
