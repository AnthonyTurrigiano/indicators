import numpy as np
import pandas as pd

def candleHasUpShadow(open, close, low, shadowPct=5.0):
    candleBodyHigh = pd.Series(np.maximum(close, open))
    candleBodyLow =  pd.Series(np.minimum(close, open))
    candleBody = candleBodyHigh - candleBodyLow
    candleDownShadow = pd.Series(low - candleBodyLow)
    candleHasDownShadow = candleDownShadow > shadowPct / 100 * candleBody

    return pd.Series(candleHasDownShadow)
