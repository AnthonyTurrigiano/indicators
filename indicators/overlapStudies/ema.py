import pandas as pd

def ema(src, period, adjust=False, ignore_na=False):
    
    ema = pd.Series(src.ewm(span=period, adjust=adjust, ignore_na=ignore_na).mean())

    return ema
