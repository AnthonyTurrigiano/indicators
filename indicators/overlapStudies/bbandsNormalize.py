
# Description: Bollinger Bands Normalized

def bbandsNormalized( src, uBand, lBand):
    return ((src-lBand) / (uBand-lBand))* 100
