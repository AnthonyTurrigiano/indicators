import pandas as pd

def rma(src, period=9, adjust=False, ignore_na=False):
    
    rma = pd.Series(src.ewm(alpha=1/period, adjust=adjust, ignore_na=ignore_na).mean())

    return rma


