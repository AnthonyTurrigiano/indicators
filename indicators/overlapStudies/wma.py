import pandas as pd
import numpy as np

# Description: Weighted Moving Average
def wma(src, period=9):

    d = (period * (period + 1)) / 2
    weights= np.arange(1, period + 1)

    def linear(w):
        def _compute(x):
            return (w * x).sum() / d
        
        return _compute
    _close = src.rolling(window=period)
    wma = _close.apply(linear(weights), raw=True)

    return pd.Series(wma)  

