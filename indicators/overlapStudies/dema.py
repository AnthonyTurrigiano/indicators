import pandas as pd

def dema(src, period=9, adjust=False, ignore_na=False):

    ema1 = src.ewm(span=period, adjust=adjust, ignore_na=ignore_na).mean()
    ema2 = ema1.ewm(span=period, adjust=adjust, ignore_na=ignore_na).mean()
    dema = pd.Series(2 * ema1-ema2)

    return dema
