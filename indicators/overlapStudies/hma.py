import numpy as np
import pandas as pd
import math

def wma(src, period=9):

    d = (period * (period + 1)) / 2
    weights= np.arange(1, period + 1)

    def linear(w):
        def _compute(x):
            return (w * x).sum() / d
        
        return _compute
    _close = src.rolling(window=period)
    wma = _close.apply(linear(weights), raw=True)

    return pd.Series(wma)  


def hma(src, period=9):
    
    halfLength = int(period/2)
    sqrtLength = int(math.sqrt(period))

    wmaf = wma(src, halfLength)
    wmas = wma(src, period)
    deltawma = 2 * wmaf - wmas
    hma = wma(deltawma, sqrtLength)

    return pd.Series(hma)
