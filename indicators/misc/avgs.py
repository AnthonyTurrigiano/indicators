def avgs(src, period1=5, period2=10, period3=20):
    
            h1 = src.rolling(window=period1).max()
            h2 = src.rolling(window=period2).max()
            h3 = src.rolling(window=period3).max()

            l1 = src.rolling(window=period1).min()
            l2 = src.rolling(window=period2).min()
            l3 = src.rolling(window=period3).min()

            

            avgh = (h1+h2+h3) / 3
            avgl = (l1+l2+l3) / 3
            avg = (avgh+avgl) / 2

            return [avg, avgh,avgl]
