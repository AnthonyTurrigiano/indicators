import numpy as np

def aroon(high, low, period=14):
      aroon_up = high.rolling(window=period+1).apply(lambda x: x.argmax(),raw=True) / period * 100
      aroon_dn = low.rolling(window=period+1).apply(lambda x: x.argmin(),raw=True) / period * 100

      return [aroon_up, aroon_dn]
