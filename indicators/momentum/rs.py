import pandas as pd

# Description: Relative Strength
def rs(src, period=14):

  # Calculate the difference in price from previous bar
  delta = src.diff()

  # Copy delta
  up, down = delta.copy(), delta.copy()

  # If up is less than 0 up equals 0
  up[up <  0] = 0

  # If down is less than 0 down equals 0
  down[down > 0] = 0

  # up and down moving averages
  gain = up.ewm(alpha=1.0/period, adjust=True).mean()
  loss = down.abs().ewm(alpha=1.0/period, adjust=True).mean()

  # Relative Strength
  rs = gain / loss
  
  return rs
