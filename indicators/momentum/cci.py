import pandas as pd
import numpy as np

def cci(src, period=20, constant=0.015):

    rolling = src.rolling(window=period)
    ma = rolling.mean()
    mad = rolling.apply(lambda s: abs(s - s.mean()).mean(), raw=True)
    cci = (src-ma) / (constant * mad )

    return cci
