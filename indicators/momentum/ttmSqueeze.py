import numpy as np
import pandas as pd

from indicators.statistic_functions.linearRegression import *
from indicators.utilities.normalize import normalize

def ttmSqueeze(high, low, src, uBand, uKelt, period, strict=True):

    diff = uBand-uKelt
    hh = high.rolling(window=period).max()
    ll = low.rolling(window=period).min()
    ma = src.rolling(window=period).mean()

    e1 = (((hh+ll)) / 2) + ma
    osc = linRegCurve(src - e1 / 2, period)
    alert = pd.Series(np.where((strict==True) & (diff < 0),1, 0))
    oscNorm = normalize(osc, period)

    return [osc, alert, oscNorm]
