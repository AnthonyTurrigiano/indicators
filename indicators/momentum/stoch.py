from indicators.overlap_studies import dema
from indicators.overlap_studies import tema
from indicators.overlap_studies import wma
from indicators.overlap_studies import hma

# Description: Stochastics
def stoch(src, high, low, kPeriod=14, dPeriod=3, kSmPeriod=3, adjust=False, ignore_na=False, smoothing='sma'):
    stoch = 100 * (src - lowest(low, kPeriod)) / (highest(high, kPeriod) - lowest(low, kPeriod))
    
    if(smoothing=='sma'):
        k = stoch.rolling(windw=kSmPeriod).mean()
        d = k.rolling(window=dPeriod).mean()
    elif(smoothing=='smm'):
        k = stoch.rolling(windw=kSmPeriod).median()
        d = k.rolling(window=dPeriod).median()
    elif(smoothing=='ema'):
        k = stoch.ewm(span=kSmPeriod, adjust=adjust, ignore_na=ignore_na).mean() 
        d = k.ewm(span=dPeriod, adjust=adjust, ignore_na=ignore_na).mean() 
    elif(smoothing=='dema'):
        k = dema(stoch, kSmPeriod)
        d - dema(k, dPeriod)
    elif(smoothing=='tema'):
        k = tema(stoch, kSmPeriod)
        d - tema(k, dPeriod)
    elif(smoothing=='rma'):
        k = stoch.ewm(alpha=1/kSmPeriod, adjust=adjust, ignore_na=ignore_na).mean() 
        d = k.ewm(alpha=1/dPeriod, adjust=adjust, ignore_na=ignore_na).mean()
    elif(smoothing=='wma'):
        k = wma(stoch, kSmPeriod)
        d - wma(k, dPeriod)
    elif(smoothing=='hma'):
        k = hma(stoch, kSmPeriod)
        d - hma(k, dPeriod)    

    return [k, d]
