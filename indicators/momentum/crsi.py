import pandas as pd
import numpy as np

# Local Libraries
from indicators.momentum_indicators.rsi import rsi

def crsi(src, period1=3, period2=2, period3=100):
    
    # Relative Strength Index of Close Price
    rsi3c = rsi(src, period1)
   
    #UpDown
    ud = pd.Series(np.zeros(len(src)))

    for i in range(1, len(src)):

        if(src[i] > src[i-1]):
            ud[i] = max(1, ud[i-1]+1)
        elif(src[i] < src[i-1]):
            ud[i] = min(-1, ud[i-1]-1)
        else:
            ud[i] = 0
 
    # Relative Strength Index of upDown
    rsi2ud = rsi(ud, 2)

    # Percent Change of Price
    pctChange = src.pct_change() * 100

    # Rate of Change
    roc = pctChange.rolling(window=period3).apply(lambda x: (x[-1] > x[:-1]).sum(), raw=True)
    
    # CRSI Formula
    crsi = (rsi3c + rsi2ud + roc) / 3

    return crsi
