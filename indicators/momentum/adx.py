import pandas as pd

def adx(high, low, close, period=14, adjust=False, ignore_na=False):
    tr1 = high - low
    tr2 = abs(high-close.shift(1))
    tr3 = abs(low-close.shift(1))
    tr = pd.DataFrame({'tr1':tr1,'tr2':tr2,'tr3':tr3})
    tr = tr[['tr1','tr2','tr3']].max(axis=1)
    atr = tr.ewm(alpha= 1/period, adjust=adjust, ignore_na=ignore_na).mean()

    up = high.diff()
    down = -low.diff()
    truerange = atr
    plus = 100 * pd.Series(np.where((up>down) & (up>0), up, 0)).ewm(alpha=1/period, adjust=adjust, ignore_na=ignore_na).mean() / atr
    minus = 100 * pd.Series(np.where((down>up) & (down>0), down, 0)).ewm(alpha=1/period, adjust=adjust, ignore_na=ignore_na).mean() / atr
    sum = plus + minus
    sum = pd.Series(np.where(sum==0,1,sum))
    adx = 100 * (abs(plus-minus) / sum).ewm(alpha=1/period, adjust=adjust, ignore_na=ignore_na).mean()
    
    return [adx, plus, minus]
