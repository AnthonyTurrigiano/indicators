import pandas as pd

def macd(src, faPeriod=12, slPeriod=26, siPeriod=9, adjust=False, ignore_na=False, smoothing="ema"):

    if(smoothing == 'sma'):
        fastMa = src.rolling(window=faPeriod).mean()
        slowMa = src.rolling(window=slPeriod).mean()
        macd = fastMa-slowMa
        signal = macd.rolling(window=siPeriod).mean()
    elif(smoothing == 'smm'):
        fastMa = src.rolling(window=faPeriod).median()
        slowMa = src.rolling(window=slPeriod).median()
        macd = fastMa-slowMa
        signal = macd.rolling(window=siPeriod).median()
    elif(smoothing == 'ema'):
        fastMa = src.ewm(span=faPeriod, adjust=adjust, ignore_na=ignore_na).mean()
        slowMa = src.ewm(span=slPeriod, adjust=adjust, ignore_na=ignore_na).mean()
        macd = fastMa-slowMa
        signal = macd.ewm(span=siPeriod, adjust=adjust, ignore_na=ignore_na).mean()
    elif(smoothing == 'dema'):
        fastMa = dema(src, faPeriod, adjust=adjust, ignore_na=ignore_na)
        slowMa = dema(src, slPeriod, adjust=adjust, ignore_na=ignore_na)
        macd = fastMa-slowMa
        signal = dema(macd, siPeriod, adjust=adjust, ignore_na=ignore_na)
    elif(smoothing == 'tema'):
        fastMa = tema(src, faPeriod, adjust=adjust, ignore_na=ignore_na)
        slowMa = tema(src, slPeriod, adjust=adjust, ignore_na=ignore_na)
        macd = fastMa-slowMa
        signal = tema(macd, siPeriod, adjust=adjust, ignore_na=ignore_na)
    elif(smoothing == 'rma'):
        fastMa = src.ewm(alpha=1/faPeriod, adjust=adjust, ignore_na=ignore_na).mean()
        slowMa = src.ewm(alpha=1/slPeriod, adjust=adjust, ignore_na=ignore_na).mean()
        macd = fastMa-slowMa
        signal = macd.ewm(alpha=1/siPeriod, adjust=adjust, ignore_na=ignore_na).mean()
    elif(smoothing == 'wma'):
        fastMa = wma(src, faPeriod)
        slowMa = wma(src, slPeriod)
        macd = fastMa-slowMa
        signal = wma(macd, siPeriod)
    elif(smoothing == 'hma'):
        fastMa = hma(src, faPeriod)
        slowMa = hma(src, slPeriod)
        macd = fastMa-slowMa
        signal = hma(macd, siPeriod)
    
    hist = pd.Series(macd-signal)
    macd = pd.Series(macd)
    signal = pd.Series(signal)


    return [macd, signal, hist]
