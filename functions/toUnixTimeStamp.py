import time 

def toUnixTimestamp(date, delta = 0):
    return int(
        time.mktime(
            date.timetuple()
        )  + delta
    )
