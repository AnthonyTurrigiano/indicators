import pandas as pd
import requests 

def companySharesOutStanding(ticker):
    url = 'https://query1.finance.yahoo.com/v7/finance/quote?symbols=' + ticker
    data = requests.get(url).json()
    data = data['quoteResponse']['result'][0]['sharesOutstanding']

    sharesOutstanding = pd.Series()
    
    sharesOutstanding['Ticker'] = ticker
    sharesOutstanding['SharesOutstanding'] = data

    return pd.DataFrame(sharesOutstanding).transpose()
