import pandas as pd
import requests 

def quoteData(ticker, startDate, endDate, interval):

    # Timeline    
    startDate = pd.to_datetime(startDate)
    endDate = pd.to_datetime(endDate)

    # Convert start and end date to unix timestamps
    startDate = toUnixTimestamp(startDate)
    endDate = toUnixTimestamp(endDate)

    # Base URL
    baseURL = 'https://query1.finance.yahoo.com//v8/finance/chart/'

    # Construct URL
    events = 'history'
    url = baseURL + ticker + '?period1=' + str(startDate) + '&period2=' + str(endDate) + '&interval=' + interval + '&events=' + events
    data = requests.get(url).json()

    return data
