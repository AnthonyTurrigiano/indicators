import pandas as pd
import requests

def companySector(ticker):

    url = 'https://query2.finance.yahoo.com/v10/finance/quoteSummary/' + ticker + '?formatted=true&lang=en-US&region=US&modules=summaryProfile%2CfinancialData%2CrecommendationTrend%2CupgradeDowngradeHistory%2Cearnings%2CdefaultKeyStatistics%2CcalendarEvents&corsDomain=finance.yahoo.com'
    data = requests.get(url).json()

    industry = data['quoteSummary']['result'][0]['summaryProfile']['industry']

    return pd.Series(industry)

