import pandas as pd
import requests 

def companyTrends(ticker):
    url = 'https://query2.finance.yahoo.com/v10/finance/quoteSummary/' + ticker + '?formatted=true&lang=en-US&region=US&modules=summaryProfile%2CfinancialData%2CrecommendationTrend%2CupgradeDowngradeHistory%2Cearnings%2CdefaultKeyStatistics%2CcalendarEvents&corsDomain=finance.yahoo.com'
    data = requests.get(url).json()
    print(url)
    # Period 0
    strongBuyP0 = data['quoteSummary']['result'][0]['recommendationTrend']['trend'][0]['strongBuy']
    buyP0 = data['quoteSummary']['result'][0]['recommendationTrend']['trend'][0]['buy']
    holdP0 = data['quoteSummary']['result'][0]['recommendationTrend']['trend'][0]['hold']
    sellP0 = data['quoteSummary']['result'][0]['recommendationTrend']['trend'][0]['sell']
    strongSellP0 = data['quoteSummary']['result'][0]['recommendationTrend']['trend'][0]['strongSell']
    
    # Period 1
    strongBuyP1 = data['quoteSummary']['result'][0]['recommendationTrend']['trend'][1]['strongBuy']
    buyP1 = data['quoteSummary']['result'][0]['recommendationTrend']['trend'][1]['buy']
    holdP1 = data['quoteSummary']['result'][0]['recommendationTrend']['trend'][1]['hold']
    sellP1 = data['quoteSummary']['result'][0]['recommendationTrend']['trend'][1]['sell']
    strongSellP1 = data['quoteSummary']['result'][0]['recommendationTrend']['trend'][1]['strongSell']

    # Period 2
    strongBuyP2 = data['quoteSummary']['result'][0]['recommendationTrend']['trend'][2]['strongBuy']
    buyP2 = data['quoteSummary']['result'][0]['recommendationTrend']['trend'][2]['buy']
    holdP2 = data['quoteSummary']['result'][0]['recommendationTrend']['trend'][2]['hold']
    sellP2 = data['quoteSummary']['result'][0]['recommendationTrend']['trend'][2]['sell']
    strongSellP2 = data['quoteSummary']['result'][0]['recommendationTrend']['trend'][2]['strongSell']

    recomendationMean = data['quoteSummary']['result'][0]['financialData']['recommendationMean']['fmt']

    trend = pd.Series()

    trend['Ticker'] = ticker
    trend['StrongBuyP0'] = strongBuyP0
    trend['BuyP0'] = buyP0
    trend['HoldP0'] = holdP0
    trend['SellP0'] = sellP0
    trend['StrongSellP0'] = strongSellP0

    trend['StrongBuyP1'] = strongBuyP1
    trend['BuyP1'] = buyP1
    trend['HoldP1'] = holdP1
    trend['SellP1'] = sellP1
    trend['StrongSellP1'] = strongSellP1

    trend['StrongBuyP2'] = strongBuyP2
    trend['BuyP2'] = buyP2
    trend['HoldP2'] = holdP2
    trend['SellP2'] = sellP2
    trend['StrongSellP2'] = strongSellP2
    
    trend['RecommendationMean'] = recomendationMean

    return pd.DataFrame(trend).transpose()
