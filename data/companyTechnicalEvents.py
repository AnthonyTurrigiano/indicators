import pandas as pd
import requests

def companyTechnicalEvents(ticker):
    url = 'https://query1.finance.yahoo.com/ws/market-analytics/v1/finance/nonsubscriber/technicalevents?tradingHorizons=all&symbol=' + ticker
    data = requests.get(url).json()
    
    shortTerm = data['technicalEvents']['shortTerm']
    midTerm = data['technicalEvents']['midTerm']
    longTerm = data['technicalEvents']['longTerm']
    support = data['technicalEvents']['support']
    resistance = data['technicalEvents']['resistance']
    stopLoss = data['technicalEvents']['stopLoss'] 
    
    events = pd.Series()
    events['Ticker'] = ticker
    events['ShortTerm'] = shortTerm
    events['MidTerm'] = midTerm
    events['LongTerm'] = longTerm
    events['Support'] = support
    events['Resistance'] = resistance
    events['StopLoss'] = stopLoss

    return pd.DataFrame(events).transpose()
