import numpy as np
import pandas as pd
import requests

def importDailyQuotes(df, tickers, startDate, endDate, dataSource='yahoo'):


  def companyProfile(ticker):
    url = 'https://query2.finance.yahoo.com/v10/finance/quoteSummary/' + ticker + '?formatted=true&lang=en-US&region=US&modules=summaryProfile%2CfinancialData%2CrecommendationTrend%2CupgradeDowngradeHistory%2Cearnings%2CdefaultKeyStatistics%2CcalendarEvents&corsDomain=finance.yahoo.com'
    data = requests.get(url).json()

    sector = data['quoteSummary']['result'][0]['summaryProfile']['sector']
    industry = data['quoteSummary']['result'][0]['summaryProfile']['industry']

    return [sector, industry]

  def companyExchange(ticker):
    
    url = 'http://d.yimg.com/autoc.finance.yahoo.com/autoc?query=' + ticker + '&region=1&lang=en'
    data = requests.get(url).json()
    companyName = data['ResultSet']['Result'][0]['name'] 
    companyExchange = data['ResultSet']['Result'][0]['exchDisp']
    return [companyName, companyExchange]

  df = pd.DataFrame()

  index = 1

  for ticker in tickers:
    try:
      print('importing', ticker, index,'of',len(tickers))
      quote = pdr.DataReader(ticker, dataSource, startDate, endDate)
      quote.reset_index(inplace=True)
      quote['ticker'] = ticker
      quote['companyName'] = companyExchange(ticker)[0]
      quote['exchange'] = companyExchange(ticker)[1]
      quote['sector'] = companyProfile(ticker)[0]
      quote['industry'] = companyProfile(ticker)[1]

      df = df.append(quote, ignore_index=True)
      index +=1
    except Exception as e:
      index +=1
      print(' - Could not import',ticker,'-' , e)
      pass
  return df
