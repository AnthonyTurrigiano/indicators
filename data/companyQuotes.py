import pandas as pd

def companyQuotes(ticker, startDate, endDate, interval):
    
    data = quoteData(ticker, startDate, endDate, interval)
    eodTimeStamp = data['chart']['result'][0]['meta']['currentTradingPeriod']['regular']['end']
   
    data =  data['chart']['result'][0]
    quote = data['indicators']['quote'][0]

    df = pd.DataFrame()

    df['Timestamp'] = data['timestamp']
    df['Date'] = pd.to_datetime(df['Timestamp'], unit='s').dt.normalize()
    df['Time'] = df.Timestamp.apply(lambda x: pd.Timestamp(x, unit='s', tz='US/Eastern').strftime('%H:%M'))
    df['Interval'] = interval
    df['Ticker'] = ticker
    df['Company'] = companyDetails(ticker)[0]
    df['Exchange'] = companyDetails(ticker)[1]
    df['Sector'] = getCompanySummary(ticker)[0]
    df['Industry'] = getCompanySummary(ticker)[1]
    df['Open'] =quote['open']
    df['High'] = quote['high']
    df['Low'] = quote['low']
    df['Close'] = quote['close']
    if( (interval == '1d') | (interval == '5d') | (interval == '1wk') | (interval == '1mo') | (interval == '3mo')):
        df['AdjClose'] = data['indicators']['adjclose'][0]['adjclose']
    df['Volume'] = quote['volume']
    df['Change'] = df.Close.diff()
    df['pctChange'] = df.Close.pct_change() * 100

    return df
