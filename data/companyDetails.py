import pandas as pd
import requests

def companyName(ticker):
    
    url = 'http://d.yimg.com/autoc.finance.yahoo.com/autoc?query=' + ticker + '&region=1&lang=en'
    data = requests.get(url).json()
    companyName = data['ResultSet']['Result'][0]['name'] 
    
    return pd.Series(companyName)
